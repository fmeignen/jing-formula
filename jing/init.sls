# -*- coding: utf-8 -*-
# vim: ft=sls
{%- from 'jing/map.jinja' import jing with context %}

unzip:
  pkg:
    - installed

jing_dist:
  archive:
    - name: /opt/jing-20091111
    - extracted
    - source: {{ jing.dist.url }}
    - archive_format: zip
    - source_hash: {{ jing.dist.hash }}
    - keep: True  # XXX
    - require:
      - pkg: unzip

/opt/jing:
  file.symlink:
    - target: /opt/jing-20091111
    - user: apache
    - group: apache
    - require:
      - pkg: apache

