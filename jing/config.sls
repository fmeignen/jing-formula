# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "jing/map.jinja" import jing with context %}

#jing-config:
#  file.managed:
#    - name: {{ jing.config }}
#    - source: salt://jing/files/example.tmpl
#    - mode: 644
#    - user: root
#    - group: root
