# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "jing/map.jinja" import jing with context %}

jing-pkg:
  pkg.installed:
    - name: {{ jing.pkg }}
