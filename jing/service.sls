# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "jing/map.jinja" import jing with context %}

jing-name:
  service.running:
    - name: {{ jing.service.name }}
    - enable: True
